import React, { useContext, useCallback, useState, useEffect } from 'react';
import Button from '../components/Button';
import './Instuctions.scss';
import { NavigationContext } from '../components/nav/NavigationProvider';
import { Page } from '../components/nav/Pages';
import TileSelector from '../components/tile-selector/TileSelector';
import random from 'lodash/random';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import words from '../data/words';
import {
  GameState,
  setGameState,
  setSelectedWord,
} from '../redux/hangmanSlice';

import './WordLength.scss';

const wordLengths = [3, 4, 5, 7, 8, 9];

const WordLength: React.FC = () => {
  const dispatch = useDispatch();
  const { navigate } = useContext(NavigationContext);

  const [selectedLength, setSelectedLength] = useState<number>(wordLengths[0]);

  const selectNewWord = useCallback(
    (length) => {
      const wordsFilteredByLength = words.filter(
        (word) => word.length === length
      );

      dispatch(
        setSelectedWord(
          wordsFilteredByLength[random(0, wordsFilteredByLength.length - 1)]
        )
      );
    },
    [dispatch]
  );

  const onSelect = useCallback(
    (value: string) => {
      const nextLength = parseInt(value, 10);
      setSelectedLength(parseInt(value, 10));
      selectNewWord(nextLength);
    },
    [selectNewWord]
  );

  const onRandomClick = useCallback(() => {
    const nextLength = wordLengths[random(0, wordLengths.length - 1)];
    setSelectedLength(wordLengths[random(0, wordLengths.length - 1)]);
    selectNewWord(nextLength);
  }, [selectNewWord]);

  const onNextClick = useCallback(() => {
    navigate(Page.Game);
    dispatch(setGameState(GameState.playing));
  }, [dispatch, navigate]);

  return (
    <div id="instructions">
      <h1 className="text-center">The Hangman</h1>

      <p className="text-center">
        Let's play <strong>Hangman</strong>! <br />
        How many letters do you want in your word?
      </p>

      <TileSelector
        size="large"
        onSelect={onSelect}
        options={wordLengths}
        selected={selectedLength.toString()}
      />

      <Button id="random" onClick={onRandomClick} label="Random" />

      <Button label="Got it!" onClick={onNextClick} primary fixed />
    </div>
  );
};

export default WordLength;
