import { useCallback, useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Hangman from '../components/hangman/Hangman';
import TileSelector from '../components/tile-selector/TileSelector';
import {
  GameState,
  setGameState,
  updateSelectedLetters,
} from '../redux/hangmanSlice';
import { RootState } from '../redux/store';

import './Game.scss';
import WordTiles from '../components/word-tiles/WordTiles';
import { Page } from '../components/nav/Pages';
import TopLink from '../components/TopLink';
import Button from '../components/Button';
import { NavigationContext } from '../components/nav/NavigationProvider';

const letters: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('');

const Game = () => {
  const dispatch = useDispatch();
  const { navigate } = useContext(NavigationContext);

  const { selectedLetters, word = '', gameState } = useSelector(
    (state: RootState) => state.hangman
  );

  const onLetterSelect = useCallback(
    (value: string) => {
      dispatch(updateSelectedLetters([...selectedLetters, value]));
    },
    [dispatch, selectedLetters]
  );

  const onNewGame = useCallback(() => {
    navigate(Page.WordLength);
    dispatch(updateSelectedLetters([]));
    dispatch(setGameState(GameState.notStarted));
  }, [dispatch, navigate]);

  const onEndGame = useCallback(() => {
    dispatch(setGameState(GameState.lost));
  }, [dispatch]);

  const faults = selectedLetters.filter((letter) => !word.includes(letter))
    .length;

  const success = word
    .split('')
    .every((letter) => selectedLetters.includes(letter));

  return (
    <div id="game">
      <div className="game-row">
        <div>
          <Hangman level={faults} />
        </div>
        <div>
          <TopLink label="Instructions" to={Page.Instructions} />
          <h1 className="text-left">The Hangman</h1>

          {gameState === GameState.lost && (
            <p style={{ color: 'red' }}>You've lost</p>
          )}

          {gameState === GameState.won && (
            <p style={{ color: 'green' }}>You've won</p>
          )}
          <p>It's a {word.length} length word</p>
          <WordTiles word={word} selectedLetters={selectedLetters} />
          <TileSelector
            options={letters}
            onSelect={onLetterSelect}
            disabledItems={selectedLetters}
            disableAll={word.length === 0 || faults > 9}
          />
        </div>
      </div>
      <div className="button-container">
        <Button label="End game" onClick={onEndGame} fixed />
        <Button label="New game" onClick={onNewGame} primary fixed />
      </div>
    </div>
  );
};

export default Game;
