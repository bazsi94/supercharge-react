import React, { useContext } from 'react';
import Hangman from '../components/hangman/Hangman';
import { NavigationContext } from '../components/nav/NavigationProvider';
import Button from '../components/Button';

import './Instuctions.scss';
import { Page } from '../components/nav/Pages';

const Instuctions: React.FC = () => {
  const { navigate } = useContext(NavigationContext);

  return (
    <div id="instructions">
      <h1 className="text-center">The Hangman</h1>

      <Hangman level={10} />

      <h2 className="text-center">Game instructions</h2>

      <p className="text-center">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed feugiat sit
        amet justo id blandit. Cras eleifend volutpat lacus, vel rutrum tortor
        ullamcorper sed.
      </p>

      <Button label="Got it!" onClick={() => navigate(Page.Game)} primary />
    </div>
  );
};

export default Instuctions;
