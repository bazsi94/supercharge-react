import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Page } from '../components/nav/Pages';

export enum GameState {
  notStarted,
  playing,
  won,
  lost,
}

interface HangmanState {
  selectedLetters: string[];
  word: string;
  gameState: GameState;
  page: Page;
}

const initialState: HangmanState = {
  selectedLetters: [],
  gameState: GameState.notStarted,
  word: '',
  page: Page.WordLength,
};

export const hangmanSlice = createSlice({
  name: 'hangman',
  initialState: initialState,
  reducers: {
    updateSelectedLetters: (state, { payload }: PayloadAction<string[]>) => {
      state.selectedLetters = payload;
    },

    setSelectedWord: (state, { payload }: PayloadAction<string>) => {
      state.word = payload;
    },
    setPage: (state, { payload }: PayloadAction<Page>) => {
      state.page = payload;
    },
    setGameState: (state, { payload }: PayloadAction<GameState>) => {
      state.gameState = payload;
    },
  },
});

export const {
  updateSelectedLetters,
  setSelectedWord,
  setPage,
  setGameState,
} = hangmanSlice.actions;

export default hangmanSlice.reducer;
