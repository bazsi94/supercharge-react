import React, { useContext } from 'react';
import { NavigationContext } from './nav/NavigationProvider';
import { Page } from './nav/Pages';
import './TopLink.scss';

interface TopLinkProps {
  label: string;
  to: Page;
}

const TopLink: React.FC<TopLinkProps> = ({ label, to }) => {
  const { navigate } = useContext(NavigationContext);

  return (
    <div className="top-link" onClick={() => navigate(to)}>
      {label}
      {'-->'}
    </div>
  );
};

export default TopLink;
