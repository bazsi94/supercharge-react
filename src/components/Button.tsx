import React from 'react';
import classnames from 'classnames';

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  label: string;
  primary?: boolean;
  fixed?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  className,
  primary = false,
  fixed = true,
  label,
  ...props
}) => {
  return (
    <button
      {...props}
      type="button"
      className={classnames(className, { fixed, primary })}
    >
      {label}
    </button>
  );
};

export default Button;
