import React from 'react';
import './WordTiles.scss';
import { isLetterPicked } from '../../lib/hangmanHelper';

interface WordTilesProps {
  word: string;
  selectedLetters: string[];
}
const WordTiles: React.FC<WordTilesProps> = ({ word, selectedLetters }) => {
  if (!word) {
    return null;
  }
  const letters = word.split('');

  return (
    <div className="word-tiles">
      {letters.map((letter: string) => (
        <div className="word-tiles__tile">
          {isLetterPicked(selectedLetters, letter) ? letter : ''}
        </div>
      ))}
    </div>
  );
};

export default WordTiles;
