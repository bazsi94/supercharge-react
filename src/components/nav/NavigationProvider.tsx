import React, { PropsWithChildren, useState } from 'react';
import { useLocalStorage } from 'react-use';
import { setPage } from '../../redux/hangmanSlice';
import './Navigation.scss';
import { Page } from './Pages';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

export interface NavigationContextType {
  navigate: (page: Page) => void;
}

export const NavigationContext = React.createContext(
  {} as NavigationContextType
);

const NavigationProvider: React.FC<PropsWithChildren<{}>> = ({ children }) => {
  const dispatch = useDispatch();

  const navigate = (page: Page) => {
    console.log(`Navigated to ${page}`);
    dispatch(setPage(page));
  };

  const { page } = useSelector((state: RootState) => state.hangman);

  const childrenArray = React.Children.toArray(children);

  return (
    <NavigationContext.Provider value={{ navigate }}>
      {childrenArray[page]}
    </NavigationContext.Provider>
  );
};

export default NavigationProvider;
