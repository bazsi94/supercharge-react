import { PropsWithChildren } from 'react';

export interface HangmanProps {
  level?: number;
}

export type HangmanRendererProps = PropsWithChildren<
  Required<Pick<HangmanProps, 'level'>>
>;
