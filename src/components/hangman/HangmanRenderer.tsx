import React from 'react';
import { HangmanRendererProps } from './types';

const HangmanRenderer: React.FC<HangmanRendererProps> = ({
  children,
  level,
}) => {
  if (level > 10 || level < 0) {
    throw Error(
      'Hangman renderer error. Level should be between 0 and 10 (inclusive)'
    );
  }

  return <>{React.Children.toArray(children).slice(0, level)}</>;
};

export default HangmanRenderer;
