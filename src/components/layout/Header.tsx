import logo from '../../assets/images/logo.png';

const Header: React.FC = () => {
  return (
    <header>
      <div className="header-inner">
        <img src={logo} id="logo" alt="Supercharge Logo" />
      </div>
    </header>
  );
};

export default Header;
