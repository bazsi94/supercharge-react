import React, { PropsWithChildren } from 'react';

const Main: React.FC<PropsWithChildren<{}>> = ({ children }) => {
  return (
    <main>
      <div className="box-container">{children}</div>
    </main>
  );
};

export default Main;
