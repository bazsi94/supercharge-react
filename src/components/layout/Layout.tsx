import Header from './Header';
import React, { PropsWithChildren } from 'react';
import Title from './Title';
import Main from './Main';

const Layout: React.FC<PropsWithChildren<{}>> = ({ children }) => {
  return (
    <>
      <Title>The Hangman</Title>
      <Header />
      <Main>{children}</Main>
    </>
  );
};

export default Layout;
