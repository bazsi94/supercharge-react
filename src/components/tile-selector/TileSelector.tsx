import './TileSelector.scss';
import Button from '../Button';
import classnames from 'classnames';

interface TileSelectorProps {
  onSelect: (value: string) => void;
  disabledItems?: string[];
  disableAll?: boolean;
  options: number[] | string[];
  size?: 'original' | 'large';
  selected?: number | string;
}

const TileSelector: React.FC<TileSelectorProps> = ({
  onSelect,
  options,
  disabledItems = [],
  disableAll = false,
  size = 'original',
  selected,
}) => (
  <div className={classnames('tile-selector', size)}>
    {options.map((value: number | string) => {
      const stringValue = value.toString();
      return (
        <Button
          key={stringValue}
          className={classnames('tile-selector__tile', {
            primary: selected?.toString() === stringValue,
          })}
          onClick={() => onSelect(stringValue)}
          disabled={disabledItems.includes(stringValue) || disableAll}
          label={stringValue}
          fixed={false}
        />
      );
    })}
  </div>
);

export default TileSelector;
