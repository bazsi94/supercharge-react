import { Provider } from 'react-redux';
import Layout from './components/layout/Layout';
import store, { persistor } from './redux/store';
import NavigationProvider from './components/nav/NavigationProvider';

import WordLength from './pages/WordLength';
import Instuctions from './pages/Instuctions';
import Game from './pages/Game';

import './assets/styles/styles.scss';
import { PersistGate } from 'redux-persist/integration/react';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Layout>
          <NavigationProvider>
            <WordLength />
            <Game />
            <Instuctions />
          </NavigationProvider>
        </Layout>
      </PersistGate>
    </Provider>
  );
};

export default App;
