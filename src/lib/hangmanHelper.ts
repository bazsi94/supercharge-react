export const isLetterPicked = (selectedLetters: string[], letter: string) =>
  selectedLetters.includes(letter);
